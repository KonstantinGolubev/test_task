import stateReducer from "./reducer/state";


let store = {
    _state: {
        sidebar: [],
        mareInfo: [],
        basePath: ''
    },

    _callSubscriber() {

    },
    getState() {
        return this._state;
    },

    subscribe(observer) {
        this._callSubscriber = observer;
    },

    dispatch(action) {
        debugger;
        this._state = stateReducer(this._state, action);
        this._callSubscriber(this._state);
    }
}

export default store;
window.store = store;