const axios = require('axios').default;
const ADD_DEFAULT_DATA = 'ADD_DEFAULT_DATA';

let initialState = {
    sidebar: [],
    mareInfo: [],
    basePath: ''
};

const stateReducer = (state = initialState, action) => {

    switch (action.type) {
        case ADD_DEFAULT_DATA: {
            axios.get('https://cors-anywhere.herokuapp.com/https://mrsoft.by/tz20/list.json')
                .then( (response) => {
                    state.sidebar = response.data.data;
                    state.basePath = response.data.basepath;
                });
            return state;
        }
        default:
            return state;
    }
}

export const addDefaultData = () => {
    return ({
        type: ADD_DEFAULT_DATA
    });
}

export default stateReducer;