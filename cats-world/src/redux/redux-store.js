import {combineReducers, createStore} from "redux";
import stateReducer from "./reducer/state";

let reducers = combineReducers({
    state: stateReducer
});

let store = createStore(reducers);

export default store;